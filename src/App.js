import React from 'react';
import './App.css';
import Weather from './Weather/Weather'
import Header from './Layout/Header'

function App() {
  return (

      <div className="App">
          <Header/>

          <div className="row">
              <div className="col s12 m6 push-m3">
                  <Weather/>
              </div>
          </div>
      </div>
  );
}

export default App;