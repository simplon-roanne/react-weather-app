import React from 'react'
import moment from 'moment';
import './Days.css'

class Days extends React.Component {
   constructor(props) {
       super(props)

       this.state = {
           currentDay : 0,
           time : moment().format('hh:mm')
       }
   }

    static getNextFiveDays() {
        moment.locale('fr')
        let days = [];

        for (let i = 0; i <= 4; i++) {
            days.push( moment().add(i, 'days').format('dddd') )
        }

        return days;
    }

    changeDay(index) {
        this.setState({currentDay: index})
        this.props.onDayChange(index);
    }

   render() {
       return (
           <div className="days">
               {Days.getNextFiveDays().map((day, index) => {
                   return (<a href="javascript:void(0)" key={index} onClick={_=>{ this.changeDay(index)}} className={this.state.currentDay === index ? 'current' : ''}>{day}</a>)
               })}
               <div className="time">{this.state.time}</div>
           </div>
       )
   }
}

export default Days