import sunny from './icons/sun.svg'
import cloudy from './icons/cloudy.svg'
import cloudySun from './icons/cloudy-sun.svg'
import rainy from './icons/rainy.svg'
import snowy from './icons/snowy.svg'
import thunder from './icons/thunder.svg'

export default {
    '01' : sunny,
    '02' : cloudySun,
    '03' : cloudy,
    '04' : cloudy,
    '09' : rainy,
    '10' : rainy,
    '11' : thunder,
    '13' : snowy,
};