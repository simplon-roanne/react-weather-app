import React from "react";
import moment from 'moment';
import './Weather.css'
import icons from './icons'
import Days from './Days'

const apiBaseUrl = 'http://api.openweathermap.org/data/2.5/forecast';
const apiKey = '58b889b5e560d3cc2ae21277d83725c5';

class Weather extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            city: '',
            geolocationCoords: null,
            fiveDaysForecast: null,
            icon : '',
            temp : '',
            wind : '',
        };

        this.loadData();
    }

    async loadData() {
        await this.enableGeolocation();
        await this.setForecast();
        this.onDayChange(0);
    }

    async setForecast() {
        let locationParam = '';

        if(this.state.geolocationCoords) {
            locationParam = 'lat='+this.state.geolocationCoords.lat+'&lon='+this.state.geolocationCoords.lng;
        }
        else {
            locationParam = 'q=Nice';
        }
        console.log(apiBaseUrl+'?appid='+apiKey+'&units=metric&'+locationParam)

        await fetch(apiBaseUrl+'?appid='+apiKey+'&units=metric&'+locationParam)
            .then(response => response.json())
            .then(json => {
                this.setState({
                    city: json.city.name,
                    fiveDaysForecast : json.list
                });
            });
    }

    async enableGeolocation() {
        if (navigator.geolocation) {
            const getLocation = _ => new Promise( (resolve, reject) => {
                navigator.geolocation.getCurrentPosition(resolve, reject);
            });

            await getLocation().then(position => {
                this.setState({geolocationCoords: {
                        lat : position.coords.latitude,
                        lng : position.coords.longitude
                    }});

                console.log(position);
            }).catch(err => {
                console.log("Geolocation not working properly : ", err)
            })
        }
        else {
            alert("Geolocation is not supported by this browser. This app won't work.");
        }
    }

    onDayChange(daysAdd) {
        const availableTimestamps = this.state.fiveDaysForecast.map(forecast => parseInt(forecast.dt));
        const currentTimestamp = moment().add(daysAdd, 'days').unix();

        const closestTimestamp = availableTimestamps.reduce((prev, curr) =>
            (Math.abs(curr - currentTimestamp) < Math.abs(prev - currentTimestamp) ? curr : prev)
        );

        const forecastIndex = availableTimestamps.indexOf(closestTimestamp);
        const currentForecast = this.state.fiveDaysForecast[forecastIndex];

        this.setState({
            icon : icons[currentForecast.weather[0].icon.substr(0,2)],
            temp : Math.round(currentForecast.main.temp),
            wind : Math.round(currentForecast.wind.speed)
        });
    }

    render() {
        return (
            <div className="weather card blue-grey darken-1">
                <div className="card-content white-text">
                    <span className="card-title">{this.state.city}</span>
                    <p>
                        <img src={this.state.icon} />
                    </p>
                    <span className="temperature">{this.state.temp}°</span>
                    <div className="wind">Vent {this.state.wind}km/h</div>
                </div>
                <div className="card-action">
                    <Days onDayChange={this.onDayChange.bind(this)} />
                </div>
            </div>
        );
    }
}

export default Weather;